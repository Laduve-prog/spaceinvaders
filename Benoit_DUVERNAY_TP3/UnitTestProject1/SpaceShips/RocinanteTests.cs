﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.SpaceShips;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.SpaceShips.Tests
{
    [TestClass()]
    public class RocinanteTests
    {
        [TestMethod()]
        public void RocinanteTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TakeDamagesTest()
        {
            Spaceship rocinanteSpaceship = new Rocinante(false);
            rocinanteSpaceship.TakeDamages(5);
            Assert.IsTrue(rocinanteSpaceship.Shield == 0 || rocinanteSpaceship.Shield == 5);
        }
    }
}