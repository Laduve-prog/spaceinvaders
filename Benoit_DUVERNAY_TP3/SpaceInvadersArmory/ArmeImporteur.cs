﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.CompilerServices;
using Extends;
using System.Runtime.Remoting;

namespace SpaceInvadersArmory
{
    internal class ArmeImporteur
    {
        private List<string> blackList;

        private Dictionary<string, int> data;

        public ArmeImporteur()
        {
            blackList = new List<string>();
            data = new Dictionary<string, int>();
        }

        public Dictionary<string,int> GetData()
        {
            return data;
        }

        public void readFile()
        {
            String line;
            try
            {
                StreamReader sr = new StreamReader("weapons.txt");
                line = sr.ReadLine();
                line = line.CustomNormalize();
                while(line != null)
                {
                    if (checkWord(1,line) == true)
                    {
                        //Ajout de la ligne au dictionnaire
                        if (data.ContainsKey(line))
                        {
                            data.TryGetValue(line, out int value);
                            data[line] = value + 1;
                        }
                        else
                        {
                            data.Add(line, 1);
                        }
                    }
                }
                sr.Close();
                AddToArmory();
            }
            catch (Exception e)
            {
                throw new Exception(message: "Cannot read file");
            }
        }

        public bool checkWord(int wordSize , string word)
        {
            bool isValid = true;
            if(word.Length > wordSize || blackList.Contains(word))
            {
                isValid = false;
            }

            return isValid;
        }

        public void AddToArmory()
        {
            foreach(KeyValuePair<string,int>word in data)
            {
                double minDamages = word.Value;
                double maxDamages = word.Value;
                Random rand = new Random();
                Array types = Enum.GetValues(typeof(EWeaponType));
                EWeaponType randType = (EWeaponType)types.GetValue(rand.Next(types.Length));
                Armory.CreatBlueprint(word.Key, randType, minDamages, maxDamages);
            }
        }
    }
}
